package lqdt.lcom.tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.*;


import lqdt.qa.common.*;
import lqdt.qa.*;
import lqdt.lcom.pages.*;
import lqdt.lcom.pages.auction.SearchPage;


public class SearchWithLoginTest extends TestBase {
	
	//Webdriver instance is already available as 'driver'
	//Properties instance already available as 'prop'
	
	DateFormat dateFormat = new SimpleDateFormat("MMddyyhhmmss");
	Date date = new Date();

	@Test
	public void KnownUserLogin(){
		
		lqdt.lcom.pages.LoginPage loginPage = new lqdt.lcom.pages.LoginPage(driver);
		loginPage.Login(prop.getProperty("smoketest.userid.value"), prop.getProperty("smoketest.password.value"));
		AssertJUnit.assertTrue(driver.getPageSource().contains("Hello, " + prop.getProperty("smoketest.userid.value")));
	}
	
	@Test(dependsOnMethods = { "KnownUserLogin" })
	public void Search()
	{
	
		HeaderPage headerPage = new HeaderPage(driver);
		SearchPage searchPage = headerPage.CompleteSearchForm(prop.getProperty("smoketest.keywords.value"), "New","West");
		AssertJUnit.assertTrue(driver.getPageSource().contains("<h2>Search Results</h2>")); 
		
		//Search Results show here
			
//				WebElement Extract_keyword = ( new WebDriverWait(driver , 10))
//						  .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//li[@id='breadcrumbCategory']/a")));

		
	}
	
	//what if we want to test multiple search conditions?... 
	
	
	@Test(dependsOnMethods = { "Search" })
	public void SaveSearch()
	{
		
		HeaderPage headerPage = new HeaderPage(driver);
		headerPage.SaveSearchAgent("SampleSearch" + dateFormat.format(date).toString());
		helper.Wait(2);
		
		AssertJUnit.assertNotNull("Saved Search Success Not Found", driver.findElement(By.className("saveSearchSuccess")));
		
	}
	
	@AfterClass
	public void logout()
	{
		lqdt.lcom.pages.LoginPage loginPage = new lqdt.lcom.pages.LoginPage(driver);
		loginPage.Logout();		
	}
	
	
	

}//Class
