package lqdt.lcom.tests;

import lqdt.qa.common.*;
import lqdt.qa.*;
import lqdt.lcom.pages.HeaderPage;
import lqdt.lcom.pages.auction.SearchPage;
import lqdt.lcom.pages.auction.ViewPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

public class BiddingTest  extends TestBase  {
	
	@Test
	public void KnownUserLogin(){
		
		lqdt.lcom.pages.LoginPage loginPage = new lqdt.lcom.pages.LoginPage(driver);
		loginPage.Login(prop.getProperty("smoketest.userid.value"), prop.getProperty("smoketest.password.value"));
		AssertJUnit.assertTrue(driver.getPageSource().contains("Hello, " + prop.getProperty("smoketest.userid.value")));
	}
	
	@Test(dependsOnMethods = { "KnownUserLogin" })
	public void Search()
	{
		
		HeaderPage headerPage = new HeaderPage(driver);
		SearchPage searchPage = headerPage.SearchAllStandardBid();
		
		//Count auctions found - if 0 then fail with appropriate 		
		if(driver.findElement(By.cssSelector("span.countClass")).getText() == "0")
		{
			//Search Returned No Items - fail or skip test
			AssertJUnit.fail("No Standard Bid Auctions Available.");
			return;
		}
		else
		{
			//Search Returns results
			ViewPage viewPage = searchPage.ViewSearchItem();//finds the first search item and clicks
			viewPage.StandardBid(prop);
			
		}
		
		
//		SearchPage searchPage = headerPage.CompleteSearchForm(prop.getProperty("smoketest.keywords.value"), "New","West");
//		AssertJUnit.assertTrue(driver.getPageSource().contains("<h2>Search Results</h2>"));
	
	}

}//Class
