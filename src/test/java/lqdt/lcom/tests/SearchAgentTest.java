package lqdt.lcom.tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import lqdt.qa.common.*;
import lqdt.qa.*;
import lqdt.lcom.pages.HeaderPage;
import lqdt.lcom.pages.account.EditSearchAgentPage;
import lqdt.lcom.pages.account.SearchAgentPage;
import lqdt.lcom.pages.auction.SearchPage;
import lqdt.lcom.pages.auction.ViewPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class SearchAgentTest extends TestBase {
	
	DateFormat dateFormat = new SimpleDateFormat("MMddyyhhmmss");
	Date date = new Date();

	@Test
	public void KnownUserLogin() {

		lqdt.lcom.pages.LoginPage loginPage = new lqdt.lcom.pages.LoginPage(driver);
		loginPage.Login(prop.getProperty("smoketest.userid.value"),prop.getProperty("smoketest.password.value"));
		
		AssertJUnit.assertTrue(driver.getPageSource().contains("Hello, " + prop.getProperty("smoketest.userid.value")));
	}
	
	@Test(dependsOnMethods = { "KnownUserLogin" })
	public void Search()
	{
		HeaderPage headerPage = new HeaderPage(driver);
		SearchPage searchPage = headerPage.CompleteSearchForm(prop.getProperty("smoketest.keywords.value"), "New","West");
		
		AssertJUnit.assertTrue(driver.getPageSource().contains("<h2>Search Results</h2>")); 		
	}
	
	@Test(dependsOnMethods = { "Search" })
	public void SaveSearch()
	{
		HeaderPage headerPage = new HeaderPage(driver);
		headerPage.SaveSearchAgent("SampleSearch" + dateFormat.format(date).toString());
		helper.Wait(2);
		
		AssertJUnit.assertNotNull("Saved Search Success Not Found", driver.findElement(By.className("saveSearchSuccess")));
	}
	
	@Test(dependsOnMethods = { "SaveSearch" })
	public void EditSearchAgent() {

		SearchAgentPage searchAgentPage = new SearchAgentPage(driver);
		EditSearchAgentPage editSearchAgentPage = searchAgentPage.EditAgent();
		searchAgentPage = editSearchAgentPage.EditAgentFormValues();
		//TODO Need to assert test here
	}
	
	@Test(dependsOnMethods = { "KnownUserLogin" })
	public void RunSearchAgent() {
		SearchAgentPage searchAgentPage = new SearchAgentPage(driver);
		SearchPage searchPage = searchAgentPage.RunAgent();
		
		AssertJUnit.assertTrue(driver.getPageSource().contains("<h2>Search Results</h2>")); 
	}
	
	@Test(dependsOnMethods = { "KnownUserLogin" })
	public void DeleteSearchAgent() {

		SearchAgentPage searchAgentPage = new SearchAgentPage(driver);
		searchAgentPage.DeleteAgent();
		
		Helper.Wait(10);

		// #genericMsg - Your Search Agent has been deleted.
		AssertJUnit.assertTrue(driver.getPageSource().contains("Your Search Agent has been deleted."));

	}



}
