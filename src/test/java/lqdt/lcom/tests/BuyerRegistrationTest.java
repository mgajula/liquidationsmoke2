package lqdt.lcom.tests;


import java.util.Properties;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.*;

import lqdt.qa.common.*;
import lqdt.qa.*;

import lqdt.lcom.pages.*;
import lqdt.lsiadmin.pages.*;
import lqdt.lsiadmin.pages.cgi_bin.*;


//Test specific imports
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class BuyerRegistrationTest extends TestBase {
		
	//Webdriver instance is already available as 'driver'
	//Properties instance already available as 'prop'
	
	
	//Some input items needed to generate username for the test
	DateFormat dateFormat = new SimpleDateFormat("MMddyyhhmmss");
	Date date = new Date();
	String buyerUserName;
	
  @Test
  public void RegisterBuyer() { 
	  
	  buyerUserName = "LBuyer"+ dateFormat.format(date).toString();
	  
	  Reporter.log("This is where the test case # belongs");
	    
	  RegisterPage registerPage = new RegisterPage(driver);
	  registerPage.CompleteRegistrationForm(prop, buyerUserName);
	  
	  // Normal assertion - this is all you need to do
	  //AssertJUnit.assertTrue(driver.getPageSource().contains("You're almost there!")); 
	  
	  //Advanced Assertion Pattern - provides more detail
	  if(driver.getPageSource().contains("You're almost there!"))
	  {
		  AssertJUnit.assertTrue(driver.getPageSource().contains("You're almost there!")); 
	  }else
	  {
		  //Report the detailed error message back in the fail
		  AssertJUnit.fail(driver.findElement(By.className("formContainer")).getText());
	  }//if
	  
  }//RegisterBuyer
  
  
  @Test(dependsOnMethods = { "RegisterBuyer" })
  public void ActivateBuyer(){
	  
	  Reporter.log("This is where the test case # belongs");

	  lqdt.lsiadmin.pages.LoginPage loginPage = new lqdt.lsiadmin.pages.LoginPage(driver);
	  loginPage.Login(prop);
	  
	  
	  
	  lqdt.lsiadmin.pages.cgi_bin.UsersPage usersPage = new UsersPage(driver);
	  usersPage.SearchUsers(buyerUserName);
	  usersPage.SelectResultsFirstUser();
	  usersPage.ActivateSelectedUser();
	  
//	  //Verify user status listed as active
//	  usersPage = new UsersPage(driver);
//	  usersPage.SearchUsers(buyerUserName);
//	  usersPage.SelectResultsFirstUser();
	 	  
	  AssertJUnit.assertTrue(driver.getPageSource().contains("Active"));
 
	  
  }//ActivateBuyer
  
  @Test(dependsOnMethods = {"ActivateBuyer"})
  public void LoginBuyer(){
	  
	  Reporter.log("This is where the test case # belongs");
	  
	  lqdt.lcom.pages.LoginPage loginPage = new lqdt.lcom.pages.LoginPage(driver);
	  loginPage.Login(buyerUserName, prop.getProperty("smoketest.password.value"));
	  
	  //need to assert login was successful
	  //Hello, LBuyer030514033321! 
	  AssertJUnit.assertTrue(driver.getPageSource().contains("Hello, " + buyerUserName));
	  
	  loginPage.Logout();
	  
	  
  }
  
 
}
