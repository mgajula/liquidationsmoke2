package lqdt.lcom.tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.*;

import lqdt.qa.common.*;
import lqdt.qa.*;

import lqdt.lcom.pages.*;
import lqdt.lcom.pages.auction.SearchPage;

public class AdvancedSearchWithLoginTest extends TestBase {

	DateFormat dateFormat = new SimpleDateFormat("MMddyyhhmmss");
	Date date = new Date();
	
	
	@Test
	public void KnownUserLogin(){
		
		lqdt.lcom.pages.LoginPage loginPage = new lqdt.lcom.pages.LoginPage(driver);
		loginPage.Login(prop.getProperty("smoketest.userid.value"), prop.getProperty("smoketest.password.value"));
		AssertJUnit.assertTrue(driver.getPageSource().contains("Hello, " + prop.getProperty("smoketest.userid.value")));
	}
	
	@Test(dependsOnMethods = { "KnownUserLogin" })
	public void AdvancedSearch()
	{
		HeaderPage headerPage = new HeaderPage(driver);
		headerPage.CompleteAdvancedSearchForm(prop.getProperty("smoketest.advancekeywords.value"));
		
		WebDriverWait wait = new WebDriverWait(driver, 20); // wait for max of 5 seconds
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".headerContainer h2")));
		
		AssertJUnit.assertTrue(driver.getPageSource().contains("<h2>Search Results</h2>")); 
	}
	
	
	@Test(dependsOnMethods = { "AdvancedSearch" })
	public void SaveSearch()
	{
		
		HeaderPage headerPage = new HeaderPage(driver);
		headerPage.SaveSearchAgent("Sample Advanced Search" + dateFormat.format(date).toString());
		
		WebDriverWait wait = new WebDriverWait(driver, 20); // wait for max of 5 seconds
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("saveSearchSuccess")));
		
		AssertJUnit.assertNotNull("Saved Search Success Not Found", driver.findElement(By.className("saveSearchSuccess")));
		
	}
	
	
	@Test(dependsOnMethods = { "AdvancedSearch" })
	public void AddItemsToWatchList()
	{
		
		//Need to get the item that was saved to watchlist
		
		//Then build the account/main page
		
		//https://stage.liquidation.com/account/main?tab=WatchList#WatchListAnchor
		
		//Then verify item shows on watchlist
		
	}

}//class
