package lqdt.lcom.pages;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import lqdt.qa.common.*;
import lqdt.qa.*;


public class RegisterPage extends PageBase {

	public RegisterPage(WebDriver driver) {
		super(driver);//Required
		
		//need to wrap navigation in if assertion
		driver.navigate().to(Global.getBaseUrl() + "/register");
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(how=How.ID, using = "tryme")
	private WebElement firstName;
	private WebElement lastName;
	private WebElement companyName;	
	private WebElement companyTitle;//Dropdown
	private WebElement address1;
	private WebElement city;
	private WebElement stateUS;//Dropdown
	private WebElement postalCode;
	private WebElement phoneNumber;
	private WebElement email;
	private WebElement email2;
	private WebElement BU; //RadioButton
	private WebElement busType;//Business Type
	private WebElement howHeard;//How you heard about us
	private WebElement username;
	private WebElement password;
	private WebElement password2;
	private WebElement register;//Create Account Button
	@FindBy(how=How.CSS, using="suc-msgContainer")
	public WebElement msgContainer;
	
	
	public RegisterPage CompleteRegistrationForm(Properties prop, String buyerUserId)
	{
 
		logger.info("Registration Form Started");
		
		firstName.sendKeys(prop.getProperty("smoketest.bfirstname.value"));
		lastName.sendKeys(prop.getProperty("smoketest.blastname.value"));
		companyName.sendKeys(prop.getProperty("smoketest.bcompanyname.value"));
		
		address1.sendKeys(prop.getProperty("smoketest.baddress1.value"));
		city.sendKeys(prop.getProperty("smoketest.bcity.value"));
		
		postalCode.sendKeys(prop.getProperty("smoketest.bzipcode.value"));
		phoneNumber.sendKeys(prop.getProperty("smoketest.bphonenumber.value"));
		email.sendKeys(buyerUserId + (prop.getProperty("smoketest.email.value")));
		email2.sendKeys(buyerUserId + (prop.getProperty("smoketest.confirmemail.value")));
		
		username.sendKeys(buyerUserId);
		logger.info("New UserName: " + buyerUserId);
		
		password.sendKeys(prop.getProperty("smoketest.password.value"));
		password2.sendKeys(prop.getProperty("smoketest.password.value"));
		
		BU.click();// Radiobutton - I intend to buy assets through Liquidation.com
		
		Select sCompanyTitle = new Select(companyTitle);
		sCompanyTitle.selectByVisibleText(prop.getProperty("smoketest.bcompanytitle.value"));
			
		Select sStateUS = new Select(stateUS);
		sStateUS.selectByVisibleText(prop.getProperty("smoketest.bstate.value"));
			
		Select sBusType = new Select(busType);
		sBusType.selectByVisibleText(prop.getProperty("smoketest.bbusinessnature.value"));
			
		Select sHowHeard = new Select(howHeard);
		sHowHeard.selectByVisibleText(prop.getProperty("smoketest.bfindus.value"));
		
		logger.info("Registration Form Complete");
			
		register.click();
		
		Helper.Wait(5);
		
		// Return the current page object as this action doesn't navigate to a page represented by another PageObject
        return this;  
	}
	
	
	
	
	
}//Class
