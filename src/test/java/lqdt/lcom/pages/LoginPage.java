package lqdt.lcom.pages;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lqdt.qa.common.*;
import lqdt.qa.*;

public class LoginPage extends PageBase {

	public LoginPage(WebDriver driver) {
		super(driver);
		
		Logout();
		
		driver.navigate().to(Global.getBaseUrl() + "/login");
		PageFactory.initElements(driver, this);
	}
	
	private WebElement loginId;//username
	private WebElement loginPwd;//password
	private WebElement loginEnter;//login button
 
	@FindBy(how=How.LINK_TEXT, using = "Forgot password or username?")
	private WebElement forgotPassword;
	
	@FindBy(how=How.CLASS_NAME,using = "bk-register")
	private WebElement register;

	
	
	public LoginPage Login(String strUserName, String strPassword)
	{
 
				
		loginId.sendKeys(strUserName);
		loginPwd.sendKeys(strPassword);
		loginEnter.click();
		Helper.Wait(10);
		return this;
		
	}
	
	public LoginPage Logout()
	{
		
		        //Check to see if session still exists and logout
				boolean logoutExists = driver.findElements( By.linkText("logout") ).size() != 0;
				if(logoutExists)
				{
					System.out.println("Logout Exists - Session still active. Logging out.");
					driver.findElement(By.linkText("logout")).click();
					Helper.Wait(10);
				}
		
		return this;
	}
	
	
	
}//Class
