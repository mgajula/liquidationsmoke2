package lqdt.lcom.pages.auction;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import lqdt.qa.common.*;
import lqdt.qa.*;

public class BidPage extends PageBase {

	public BidPage(WebDriver driver) {
			super(driver);//Required
			
			//need to wrap navigation in if assertion
			//driver.navigate().to(Global.getBaseUrl() + "/register");
			PageFactory.initElements(driver, this);
	}

}
