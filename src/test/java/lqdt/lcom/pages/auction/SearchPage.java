package lqdt.lcom.pages.auction;


import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import lqdt.qa.common.*;
import lqdt.qa.*;

public class SearchPage extends PageBase {

	public SearchPage(WebDriver driver) {
		super(driver);//Required
		
		//need to wrap navigation in if assertion
		//driver.navigate().to(Global.getBaseUrl() + "/auction/");
		PageFactory.initElements(driver, this);
	}
	
	
	
	//Add items to watch list
	public SearchPage AddItemsToWatchList()
	{
		
		//Add a search result item to Watch List
		System.out.println("Wait 5 Seconds");
		Helper.Wait(5);
		
		List<WebElement> lsAddToWatchList = driver.findElements(By.cssSelector("a.track-ga.itemWatchIcon"));
		System.out.println(lsAddToWatchList.size() + " Search Items Found");
		
		Integer i = 0;
		
		for(WebElement item : lsAddToWatchList ){
			   if (item.isDisplayed())
			   {
				   if (i == 5) break;
				   item.click();//Add item to watch list
				   System.out.println("Search Item Add to Saved List Displayed");
				   Helper.Wait(2);
				   i++;				   
			   }else
			   {   
				   System.out.println("Search Item Add to Saved List Not Displayed. Auction possibly closed.");
			   }
			
			}//End For Loop
		
		if(i==0)
		{
			System.out.println("No open auction items found. Nothing added to watch list.");
			
		}else
		{
			System.out.println(i + " Items added to watch list.");	
		}
		
		
		return this;
		
	}
	
	//View List Item
	public ViewPage ViewSearchItem()
	{

		//Find all the item links
		List<WebElement> lsSearchResults = driver.findElements(By.cssSelector(".itemDescription a"));
		System.out.println(lsSearchResults.size() + " Search Items Found");
		
		Integer i = 0;
		
		for(WebElement item : lsSearchResults ){
			   if (item.isDisplayed())
			   {
				   if (i == 1) break;
				   item.click();//Navigate to item
				   System.out.println(item.getText() + " Link Clicked");
				   Helper.Wait(2);
				   i++;				   
			   }
			
			}//End For Loop

		return new ViewPage(driver);
	}
	
	

}
