package lqdt.lcom.pages.auction;


import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import lqdt.qa.common.*;
import lqdt.qa.*;

public class ViewPage extends PageBase {

	public ViewPage(WebDriver driver) {
		super(driver);//Required
		
		//need to wrap navigation in if assertion
		//driver.navigate().to(Global.getBaseUrl() + "/register");
		PageFactory.initElements(driver, this);
	}
	
	//@FindBy(how=How.ID, using = "tryme")
	private WebElement bidAmount;
	private WebElement placeSingleBid;
	private WebElement card_number;
	private WebElement card_type;
	private WebElement card_owner_name;
	private WebElement card_csc;
	
	
	public ViewPage StandardBid(Properties prop)
	{
		WebDriverWait wait = new WebDriverWait(driver, 30); 
		
		bidAmount.sendKeys(prop.getProperty("smoketest.bidamount.value"));
		placeSingleBid.click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("card_number")));
		
		card_number.sendKeys(prop.getProperty("smoketest.creditcardnumber.value").trim());
		
		Select sCard_Type = new Select(card_type);
		sCard_Type.selectByVisibleText(prop.getProperty("smoketest.ccardname.value"));
		
		card_owner_name.sendKeys(prop.getProperty("smoketest.blastaname.value"));
		card_csc.sendKeys(prop.getProperty("smoketest.cardcsc.value").trim());
		
		
//		driver.findElement(By.name("bidAmount")).sendKeys(SeleniumUtil.getProperty("smoketest.bidamount.value"));
//		driver.findElement(By.name("placeSingleBid")).click();	
//		Thread.sleep(5000);	
//		driver.findElement(By.name("card_number")).sendKeys(SeleniumUtil.getProperty("smoketest.creditcardnumber.value").trim());
//		WebElement CC= driver.findElement(By.name("card_type"));
//		CC.sendKeys(SeleniumUtil.getProperty("smoketest.ccardname.value").trim());
//		driver.findElement(By.name("card_owner_name")).sendKeys(SeleniumUtil.getProperty("smoketest.blastaname.value").trim());
//		driver.findElement(By.name("card_csc")).sendKeys(SeleniumUtil.getProperty("smoketest.cardcsc.value").trim());

		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[3]/div[2]/div/form/div[2]/div[5]/select[2]/option[3]")).click();
		//driver.findElement(By.xpath("html/body/div[1]/div[2]/div[3]/div[2]/div/form/div[3]/button")).click();		
		driver.findElement(By.name("address_id")).click();
		driver.findElement(By.xpath("html/body/div[1]/div[2]/div[3]/div[2]/div/form/div[3]/button")).click();
		
		if( driver.findElement(By.name("address_id")).isDisplayed()){
			driver.findElement(By.name("address_id")).click();
			}		
		//driver.findElement(By.id("confirmBidButton")).click();//test8
		driver.findElement(By.cssSelector("button[class='confirmbid']")).click();
		
		
		return this;
	}

}
