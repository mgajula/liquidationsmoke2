package lqdt.lcom.pages;
import lqdt.qa.common.*;
import lqdt.qa.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 
import lqdt.lcom.pages.auction.SearchPage;

public class HeaderPage extends PageBase {

	public HeaderPage(WebDriver driver) {
		super(driver);
		//Header present on all pages. No need to validate navigation
		PageFactory.initElements(driver, this);
	}
	
	
	private WebElement new_words; //search term
	private WebElement new_srch_category1;//dropdown
	private WebElement new_item_condition;//dropdown
	private WebElement new_location;//dropdown
	@FindBy(how=How.ID, using = "basic-search")//dash in name caused trouble
	private WebElement btnBasicSearch;//button
	//Simple Search
	private WebElement saveSearch;
	private WebElement saveSearchText;
	private WebElement runSearchCheck;
	private WebElement saveSearchType; //Got some doubts about this - custom html select thingy
	@FindBy(how=How.CLASS_NAME, using="saveBtn")
	private WebElement saveBtn; //CSS Class name used
	
	//Advanced Search
	@FindBy(how=How.CSS, using=".advancedSearchLink a")
	private WebElement advancedSearchLink;
	private WebElement new_advSearch;
	private WebElement new_adv_words;
	private WebElement new_kyOptTitle; //Keyword option title
	private WebElement new_kyOptTitle_Des;//Keyword Option Title & description
	private WebElement new_typeoption0;//Auction Type New
	@FindBy(how=How.CSS, using="input.goButton")
	private WebElement goButton;
	
	
	//.categoryListLink a
	//.advancedSearchLink a
	
	public SearchPage CompleteSearchForm(String searchTerm, String itemCondition, String location)
	{
		
		new_words.sendKeys(searchTerm);
		
		Select sNewItemCondition = new Select(new_item_condition);
		sNewItemCondition.selectByVisibleText(itemCondition);
		
		Select sNewLocation = new Select(new_location);
		sNewLocation.selectByVisibleText(location);
		
		btnBasicSearch.click();
		
		return (new SearchPage(driver));
	}
	
	public SearchPage CompleteAdvancedSearchForm(String searchTerm)
	{
		
		WebDriverWait wait = new WebDriverWait(driver, 30); // wait for max of 5 seconds
		
		//Need to mouseover to expand the search panel
		System.out.println("Mouseover Search Panel");
		Actions action = new Actions(driver);
		action.moveToElement(advancedSearchLink);
		action.perform();
		
		
		System.out.println("Sending Keywords");
		wait.until(ExpectedConditions.visibilityOf(new_adv_words)).sendKeys(searchTerm);

		System.out.println("Clicking Condition Dropdown");
		driver.findElement(By.xpath("//*[@id='new_condition']/div[1]")).click();
		System.out.println("New Condition Click");
		
		System.out.println("Condition Dropdown Clicked");
		driver.findElement(By.id("new_conditionoption3")).click();
		System.out.println("Condition Option 3 Selected");
		driver.findElement(By.xpath("//*[@id='new_condition']/div[1]")).click();
		System.out.println("New Condition Closed");
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input.goButton"))).click();
		
		if(ExpectedConditions.visibilityOf(new_advSearch) == null) //the search form must be visible
		{
			System.out.println("Form Lost Focus - clicking on advanced search link");
			advancedSearchLink.click();
		}
		
		System.out.println("GoButton Clicked");
		
		if(ExpectedConditions.alertIsPresent() != null)
		{
			Alert alert = driver.switchTo().alert();
			alert.accept();
			System.out.println("Alert Accepted");
		}else
		{
			System.out.println("No mixed content alert displayed");
		}
		
		return (new SearchPage(driver));
		
	}
	
	public SearchPage SearchAllStandardBid()
	{
		
		WebDriverWait wait = new WebDriverWait(driver, 30); // wait for max of 30 seconds
		
		//Need to mouseover to expand the search panel
				System.out.println("Mouseover Search Panel");
				Actions action = new Actions(driver);
				action.moveToElement(advancedSearchLink);
				action.perform();
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("new_typeoption0"))).click();
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input.goButton"))).click();
				System.out.println("GoButton Clicked");
				
//				
//				if(ExpectedConditions.visibilityOf(new_advSearch) == null) //the search form must be visible
//				{
//					System.out.println("Form Lost Focus - clicking on advanced search link");
//					advancedSearchLink.click();
//				}
				
				
				
				if(ExpectedConditions.alertIsPresent() != null)
				{
					Alert alert = driver.switchTo().alert();
					alert.accept();
					System.out.println("Alert Accepted");
				}else
				{
					System.out.println("No mixed content alert displayed");
				}
				 
		
		
		return new SearchPage(driver);
	}
	
	
	public HeaderPage SaveSearchAgent(String searchName )
	{
		
		saveSearch.click();
		saveSearchText.sendKeys(searchName);			
		saveBtn.click();
		return this;
	}
	
	public HeaderPage SaveSearchAgent(String searchName, Boolean blnEmail, String strRunFrequency )
	{
		
		saveSearch.click();
		saveSearchText.sendKeys(searchName);
		
		if(blnEmail)
		{
			runSearchCheck.click();
		}
		
		Select sSaveSearchType = new Select(saveSearchType);
		sSaveSearchType.selectByVisibleText(strRunFrequency);
		
		saveBtn.click();
		
		return this;
	}
	
	

}
