package lqdt.lcom.pages.account;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import lqdt.qa.common.*;
import lqdt.qa.*;



public class WatchListPage  extends PageBase {

	public WatchListPage(WebDriver driver) {
		super(driver);//Required
		
		//need to wrap navigation in if assertion
		driver.navigate().to(Global.getBaseUrl() + "/account/main?tab=WatchList");
		PageFactory.initElements(driver, this);
	}
	
	public WatchListPage RemoveWatchListItems()
	{
		
		System.out.println("Remove items from watchlist");
		String Part1="//*[@id='watchstd']/tbody/tr[";
		String Part2="]";
		String Part3="/td[1]/input";
		//Select all check boxes
		int resultSize = driver.findElements(By.name("removeFromWatchlist")).size();
		System.out.println("Results Size:" + resultSize);
		
		for(int i=1;i<=resultSize;i++)
		{
			System.out.println(Part1+i+Part2+Part3);
			WebElement allCheckboxes=driver.findElement(By.xpath(Part1+i+Part2+Part3));
			allCheckboxes.click();
		}
		
		//Remove Auctions
		System.out.println("Remove auctions");
		WebElement deleteWatchList = driver.findElement(By.id("deleteFromWatchlistStandard"));  	
	    String jsDelete = "arguments[0].style.height='auto';arguments[0].style.visibility='visible';arguments[0].click();";
	    ((JavascriptExecutor) driver).executeScript(jsDelete, deleteWatchList);
		
		
		return this;
		
	}
	
	
	
	

}
