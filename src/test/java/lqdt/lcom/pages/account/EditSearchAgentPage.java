package lqdt.lcom.pages.account;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import lqdt.qa.common.*;
import lqdt.qa.*;

public class EditSearchAgentPage extends PageBase {

	public EditSearchAgentPage(WebDriver driver) {
		super(driver);//Required
		//no navigation - you arrive here from a link
		//need to verify url
		PageFactory.initElements(driver, this);
	}
	
	private WebElement search_name;
	private WebElement email_flagy;
	private WebElement email_flayn;
	private WebElement srch_words;
	private WebElement descripy;
	private WebElement descripn;
	private WebElement srch_words_mode; //Select
	//auction type no discreet ID
    private WebElement category1; //Select Categories
	private WebElement location;//Select 
	private WebElement username_seller;//
	
	@FindBy(how=How.CSS, using =".buttonHolder input")
	private WebElement submit;

	public SearchAgentPage EditAgentFormValues()
	{
		
	logger.info("Changing Search Agent Edit Form Values");
  	logger.info("Email notification radio button");
  	email_flagy.click();

	//Select Lot Size
	logger.info("Select lot size");
	driver.findElement(By.xpath("//*[@id='agentform']/fieldset/div[10]/select/option[1]")).click();
	//Click on Save button
	logger.info("Saving");
		
	new WebDriverWait(driver, 40).until(ExpectedConditions.visibilityOf(submit));
	
	submit.click();
	
	logger.info("Edit Agent Save clicked");

	return new SearchAgentPage(driver);
	}
	
	
	
}//class
