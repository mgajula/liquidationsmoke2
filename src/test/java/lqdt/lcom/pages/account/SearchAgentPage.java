package lqdt.lcom.pages.account;

import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;

import lqdt.lcom.pages.auction.SearchPage;
import lqdt.qa.common.*;
import lqdt.qa.*;


public class SearchAgentPage extends PageBase {

	public SearchAgentPage(WebDriver driver) {
		super(driver);//Required
		
		driver.navigate().to(Global.getBaseUrl() + "/account/main?tab=SearchAgent");
		PageFactory.initElements(driver, this);
	}


	
	private WebElement FindAgent()
	{

		//get all watchlist table rows
		List<WebElement> lsWatchList = driver.findElements(By.cssSelector(".agenttable tr"));
		logger.info(lsWatchList.size() + " Search Agents Found");
		
		Integer i = 0;
		
		for(WebElement item : lsWatchList ){
			logger.info("Watchlist Item Details: ");
			logger.info(item.findElement(By.cssSelector("th h4")).getText());
			logger.info(item.findElement(By.cssSelector("td ul li")).getText());			
//			WebElement btnRunSearchAgent = item.findElement(By.cssSelector("button.runagent"));
//			WebElement lnkEditSearchAgent = item.findElement(By.linkText("Edit"));
//			WebElement lnkDeleteSearchAgent = item.findElement(By.linkText("Delete"));

			return item; //return the first item found
			}//End For Loop
		logger.info("NO WATCHLIST ITEMS FOUND");
		return null; //nothing found
	}
	
	
	public SearchPage RunAgent()
	{
		WebElement watchListRow = FindAgent();
		//click run agent
		watchListRow.findElement(By.cssSelector("button.runagent")).click();
		logger.info("Run Search Agent Clicked");	  	
	  	return new SearchPage(driver);
	}
	
	public EditSearchAgentPage EditAgent()
	{
		WebElement watchListRow = FindAgent();
		//click edit agent
		watchListRow.findElement(By.linkText("Edit")).click();
		logger.info("Edit Search Agent Clicked");
		
		return new EditSearchAgentPage(driver);
		
	}
		
	public SearchAgentPage DeleteAgent()
	{
		
		WebElement watchListRow = FindAgent();
		//click edit agent
		watchListRow.findElement(By.linkText("Delete")).click();
		logger.info("Delete Search Agent Clicked");
			
	  	//Delete confirmation accept
		List<WebElement> buttonElementList = driver.findElements(By.cssSelector("div[id^='dialogTest_']"));	
		logger.info("Delete dialog button List contains "+buttonElementList.size() + " buttons");
		
		for(WebElement item : buttonElementList ){
			
			if(item.getText().contains("You are about to delete your"))
			{
				logger.info("Delete Confirmation Box Located");
				item.findElement(By.cssSelector("a[class='bk-delete']")).click();
				logger.info("Delete Confirmation Button Clicked");			
			}
		}
				
		return this;
		
	}
	
	
}//class
