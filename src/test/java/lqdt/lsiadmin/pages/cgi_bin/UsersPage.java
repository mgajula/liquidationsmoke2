package lqdt.lsiadmin.pages.cgi_bin;

import lqdt.qa.common.*;
import lqdt.qa.*;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;


public class UsersPage extends PageBase {

	public UsersPage(WebDriver driver) {
		super(driver);//Required
		//need to wrap navigation in if assertion
		driver.navigate().to(Global.getLsiUrl() + "/cgi-bin/users");
		//need to switch before calling init
		driver.switchTo().frame("search_users");
		PageFactory.initElements(driver, this);
	}

	
	private WebElement user_id;
	private WebElement username;
	private WebElement submit_search_user;
	
	
	public UsersPage SearchUsers(String buyerUserName)
	{	
		driver.switchTo().defaultContent();
		driver.switchTo().frame("search_users");
		
		username.sendKeys(buyerUserName);
		
//		JavascriptExecutor js = (JavascriptExecutor)driver;
//		js.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight," + "document.body.scrollHeight,document.documentElement.clientHeight));");
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(submit_search_user)).click();
		helper.Wait(5);
		// Return the current page object as this action doesn't navigate to a page represented by another PageObject
        return this;  
	}
	
	public UsersPage ActivateSelectedUser()
	{
		
		
		WebDriverWait wait = new WebDriverWait(driver, 30); // wait for max of 5 seconds
		//wait.until(ExpectedConditions.visibilityOf(advancedSearchLink)).click();
		//wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input.goButton"))).click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//th[@class='header' and text()='Status:']"))).click();
		//Select Active
		System.out.println("Selecting Active on user account");
		
		Select userStatus = new Select(driver.findElement(By.xpath("html/body/form/table[1]/tbody/tr[1]/td[1]/center/table/tbody/tr[4]/td/span[2]/select")));
		userStatus.selectByVisibleText("Active");
		
		//Save Button
		driver.findElement(By.xpath("//*[@id='save_button']")).click();
		System.out.println("Account Activated in LSI Admin");
		helper.Wait(5);
		
		 return this;  
		
	}
	
	public UsersPage SelectResultsFirstUser()
	{
		
		driver.switchTo().frame("iframe_user");
		//Click on user id 
		driver.findElement(By.xpath("//*[@id='user_row_1']/td[2]/a")).click();
		driver.switchTo().defaultContent();
		driver.switchTo().frame("detail_users");//Detail Users Frame
		
		return this;  
		
	}
	
	
	
	
}//class
