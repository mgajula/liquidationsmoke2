package lqdt.lsiadmin.pages;

import java.util.Properties;

import lqdt.lsiadmin.pages.cgi_bin.AdminPage;
import lqdt.qa.common.*;
import lqdt.qa.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;


public class LoginPage extends PageBase {

	public LoginPage(WebDriver driver) {
		super(driver);//Required
		//need to wrap navigation in if assertion
		driver.navigate().to(Global.getLsiUrl() + "/login");
		PageFactory.initElements(driver, this);
	}
	
	private WebElement office;
	private WebElement username;
	private WebElement password;
	
	@FindBy(how=How.CSS, using ="input[type=\"submit\"]")
	private WebElement submit;
	
	
	public AdminPage Login(Properties prop)
	{
		username.sendKeys(prop.getProperty("smoketest.wusername.value"));
		password.sendKeys(prop.getProperty("smoketest.wpassword.value"));
		submit.click();
		logger.info("LSI Admin Login Button Clicked");
		
		helper.Wait(5);
	
		
		return new AdminPage(driver);
	}

}
