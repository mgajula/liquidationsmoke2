package lqdt.qa.common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * @author wpballard
 *
 */
public class PageBase { 
	
	public WebDriver driver;
	public Helper helper;
	protected final static  Logger logger = LoggerFactory.getLogger(PageBase.class);
	
	public PageBase(WebDriver driver) {
		this.driver = driver;
	}
	

}//class
