package lqdt.qa.common;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Helper {
	
	//private WebDriver driver;
	 final static Logger logger = LoggerFactory.getLogger(Helper.class);

//	public PageSeleniumHelper(WebDriver driver) {
//			this.driver = driver;
//	}
	

	
	
	public static void Wait(Integer intSeconds)
	{
		logger.info("Waiting " + intSeconds + " Seconds");

		Thread thread = new Thread();
		try {
			
			synchronized(thread){
				thread.wait(intSeconds * 1000);
			}
		}
		catch (InterruptedException e)
		{
			
		}
		
		logger.info(intSeconds + " Wait Complete");
	}
	
	
//	protected boolean isElementPresent(By by,WebDriver driver) {
//		try {
//			 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//			return driver.findElement(by).isEnabled();
//		} catch (NoSuchElementException e) {
//			System.out.println("Element is missing "+e);
//			return false;
//		} 
//	}

}
