package lqdt.qa.common;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestListener;
import org.testng.IExecutionListener;

public class TestNGExecutionListener implements IExecutionListener {
	
	
	public WebDriver driver;
	Process gridShellProcess;
	Process gridNodeProcess;
	
	final Logger logger = LoggerFactory.getLogger(TestNGSuiteListener.class);
	
//	public TestNGExecutionListener() {
//		// TODO Auto-generated constructor stub
//	}

	@Override
	public void onExecutionStart() {
		
		if(LocalDriverManager.getDriver() != null)
		{
			logger.info("Driver Already Initialized, exiting method.");
			 return; //exit method
		}
		

		logger.info("Testing Started");
		
		// Choose the browser, version, and platform to test
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setBrowserName(Global.getBrowser());
		capabilities.setCapability("version", Global.getBrowserVersion());
		capabilities.setCapability("platform", Platform.valueOf(Global.getOS()));
		
		//Set report title to actual URL - may change this
		System.setProperty("org.uncommons.reportng.title", Global.getBaseUrl());

		if (Global.getBlnUseSauce()) { // Create the connection to Sauce Labs to run the
							// tests
			try {
				
				driver = new RemoteWebDriver(new URL("http://" + Global.getUserName() + ":" + Global.getKey() + "@ondemand.saucelabs.com:80/wd/hub"),
						capabilities);
				logger.info("Current Test URL: " + Global.getBaseUrl());
				LocalDriverManager.setWebDriver(driver);//make it threadsafe
				
			} catch (MalformedURLException e) {
				e.printStackTrace();
				//logger.error(e.)
			}
			//capabilities.setCapability("name", method.getName()); //Commented out during dev, may need to reinstate
		} else {
			// Create the connection to LQDT Selenium Grid Server
			// http://10.65.85.172:4444/grid/console
			try {
				
				
//				StartWebGrid();
//				logger.info("Initializing RemoteWebDriver");
//				this.driver = new RemoteWebDriver(new URL(gridUrl), capabilities);
				logger.info("Initializing Local Firefox WebDriver");
				driver = new FirefoxDriver();
				driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	//			WebDriverEventListener webDriverListener = new WebDriverEventListener();
				
				
				
//				MyRemoteWebDriver rwd = new MyRemoteWebDriver(new URL(MyConstants.URL.toString()), dc);
//				EventFiringWebDriver efwd = new EventFiringWebDriver(rwd);
//				WebDriverListener eventListener = new WebDriverListener(rwd);
//				efwd.register(eventListener);
//				
				logger.info("Selenium Grid Test");
				logger.info("Current Test URL: "+ Global.getBaseUrl());
				logger.info("Browser: " + capabilities.getBrowserName() + " " + capabilities.getVersion());
				driver.navigate().to(Global.getBaseUrl());
				driver.manage().window().maximize();
				LocalDriverManager.setWebDriver(driver);//make it threadsafe
			
//			} catch ( InterruptedException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			}
		    catch (Exception e) {
			e.printStackTrace();
		    }
		}
		
		
	}

	@Override
	public void onExecutionFinish() {
		
		if( LocalDriverManager.getDriver() != null)
		{
			driver = LocalDriverManager.getDriver();//Gets a threadsafe instance
			driver.close();
			driver.quit();	
			try {
				EndWebGrid();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			LocalDriverManager.setWebDriver(null);
			
		}
		

		
		logger.info("All Tests Completed");
	}

	
	
	//Start WebGrid 
	public void StartWebGrid() throws IOException, InterruptedException
	{
		
		//Start WebGrid
		logger.info("Starting Selenium Grid Server");
		
		
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command("java", "-jar", "src/test/resources/selenium-server-standalone-2.39.0.jar", "-role", "hub");
 
		
		gridShellProcess=processBuilder.start();
		logger.info("Selenium Grid Server Started");

		logger.info("Wait 10 Seconds for grid to initialize");
		Helper.Wait(10);
		logger.info("Wait completed");		
		logger.info("Selenium Grid Server browseable at http://localhost:4444/grid/console");

		
		logger.info("Starting Selenium Grid Node");
		//Start the node and attach to grid

		processBuilder.command("java","-jar", "src/test/resources/selenium-server-standalone-2.39.0.jar", "-role", "node","-hub",  "http://localhost:4444/grid/register");

		gridNodeProcess = processBuilder.start();
		logger.info("Selenium Grid Grid Node Started");
		logger.info("Wait 10 Seconds for node to initialize");
		Helper.Wait(10);
		logger.info("Wait completed");

		
	}
	
	
	public void EndWebGrid() throws InterruptedException
	{
//		gridNodeProcess.destroy();
//		logger.info("Wait 5 Seconds for Grid Node to destroy");
//		Wait(5);
//		logger.info("Selenium Grid Node Destroyed");
//		logger.info("Wait 5 Seconds for Grid Shell to destroy");
//		Wait(5);
//		gridShellProcess.destroy();	
//		logger.info("Selenium Grid Shell Destroyed");
	}
	
	
	
}//Class
