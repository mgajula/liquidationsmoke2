package lqdt.qa.common;

import org.testng.ISuite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Global {
	
	
	private static String userName = "userName";
	private static String key = "key";
	private static String os = "WINDOWS";
	private static String browser = "firefox";
	private static String browserVersion = null;
	private static Boolean blnUseSauce = false;
	private static String baseUrl = "http://test3.liquidation.com";
	private static String lsiUrl = "http://test3.lsiadmin.com";
	private static String gridUrl = "http://localhost:4444/wd/hub";
    
    
    final static Logger logger = LoggerFactory.getLogger(Global.class);

	public Global() {}
	
	
	public static String getUserName(){
		if (System.getProperty("userName") != null)
		{	
			userName = System.getProperty("userName");
		}
		return userName;
	}
	
	public static String getKey() {
		if (System.getProperty("key") != null)
		{
			key = System.getProperty("key");
		}
		return key;
	}
	
	public static String getOS(){
		if (System.getProperty("os") != null)
		{
			os = System.getProperty("os");
			logger.info("OS PARAMETER FOUND: " + os );
		}
		return os;
	}
	
	public static String getBrowser(){
		if (System.getProperty("browser") != null)
		{
			browser = System.getProperty("browser");
			logger.info("BROWSER PARAMETER FOUND: " + browser );
		}
		return browser;
	}
	
	public static String getBrowserVersion(){
		if (System.getProperty("browserVersion") != null)
		{
			browserVersion = System.getProperty("browserVersion");
			logger.info("BROWSER VERSION PARAMETER FOUND: " + browserVersion );
		}
		return browserVersion;
	}
	
	public static boolean getBlnUseSauce(){
		if (System.getProperty("blnUseSauce") != null)
		{
			blnUseSauce = Boolean.valueOf(System.getProperty("blnUseSauce"));
			logger.info("USESAUCE PARAMETER FOUND: " + blnUseSauce );
		}
		return blnUseSauce;
	}
	
	public static String getBaseUrl()
	{
		if (System.getProperty("baseUrl") != null)
		{
			baseUrl = System.getProperty("baseUrl");
			logger.info("BASEURL PARAMETER FOUND: " + baseUrl );
		}
		return baseUrl;
	}
	
	public static String getLsiUrl(){
		if (System.getProperty("lsiUrl") != null)
		{
			lsiUrl = System.getProperty("lsiUrl");
			logger.info("LSIURL PARAMETER FOUND: " + lsiUrl );
		}
		return lsiUrl;
	}
	
	public static String getGridUrl(){
		if (System.getProperty("gridUrl") != null)
		{
			gridUrl = System.getProperty("gridUrl");
			logger.info("GRIDURL PARAMETER FOUND: " + gridUrl );
		}
		return gridUrl;
	}
	
}//class
